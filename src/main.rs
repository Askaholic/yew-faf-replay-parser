extern crate yew;
extern crate fafreplay;

use yew::services::reader::{File, FileData, ReaderService, ReaderTask};
use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender, ChangeData};
// use fafreplay::ReplayReadError;
use fafreplay::scfa::{Replay, Parser, ParserBuilder, replay};
use fafreplay::faf::extract_scfa;


fn main() {
    yew::start_app::<Model>();
}

struct Model {
    link: ComponentLink<Model>,
    reader: ReaderService,
    tasks: Vec<ReaderTask>,
    replay: Option<Replay>,
    error: Option<String>,
    parser: Parser,
}

enum Msg {
    None,
    File(File),
    FileLoaded(FileData),
    Err(String),
    ClearErr,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Model {
        use replay::replay_command::*;

        Model {
            link,
            reader: ReaderService::new(),
            tasks: vec![],
            replay: None,
            error: None,
            parser: ParserBuilder::new()
                .commands(&[
                    ADVANCE,
                    VERIFY_CHECKSUM,
                    LUA_SIM_CALLBACK
                ])
                .save_commands(false)
                .build(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::None => return false,
            Msg::File(f) => {
                let callback = self.link.send_back(Msg::FileLoaded);
                let name = f.name();
                if name.ends_with(".scfareplay") || name.ends_with(".fafreplay") {
                    self.tasks.push(self.reader.read_file(f, callback));
                }
            },
            Msg::FileLoaded(data) => {
                let content = {
                    if data.name.ends_with(".fafreplay") {
                        match extract_scfa(&mut data.content.as_slice()) {
                            Ok(content) => content,
                            Err(e) => {
                                self.link.send_self(Msg::Err(format!("{:?}", e)));
                                return false;
                            }
                        }
                    } else {
                        data.content
                    }
                };
                match self.parser.parse(&mut content.as_slice()) {
                    Ok(replay) => self.replay = Some(replay),
                    Err(e) => {
                        self.link.send_self(Msg::Err(format!("{:?}", e)));
                        return false;
                    }
                }
            },
            Msg::Err(s) => self.error = Some(s),
            Msg::ClearErr => self.error = None,
        }
        true
    }
}

impl Renderable<Model> for Model {
    fn view(&self) -> Html<Model> {
        html!(
            <div class="yew-page-wrap-hack">
                <div class="all-content">
                    { self.render_error() }
                    <section class="page-wrap">
                        <header class="header">
                            <h1>{ "FAF Replay Analyzer" }</h1>
                        </header>
                        { self.render_page() }
                    </section>
                    <footer class="footer">
                        <section class="footer-content">
                            <small>{ "Copyright © Askaholic 2019" }</small>
                        </section>
                    </footer>
                </div>
            </div>
        )
    }
}

impl Model {
    fn render_page(&self) -> Html<Model> {
        match self.replay {
            None => html!(
                <main class="main">
                    <section class="browse">
                        { self.render_upload_button() }
                    </section>
                </main>
            ),
            Some(ref replay) => html!(
                <main class="main">
                    { self.render_upload_button() }
                    <p>{ &replay.header.scfa_version }</p>
                </main>
            )
        }
    }

    fn render_upload_button(&self) -> Html<Model> {
        html!(
            <section class="browse-button">
                <label for="input_replay">{ "Replay File" }</label>
                <input id="input_replay" type="file" accept=".fafreplay,.SCFAReplay"
                    onchange=|value| {
                        if let ChangeData::Files(files) = value {
                            if files.len() > 0 {
                                return Msg::File(files.iter().next().unwrap());
                            }
                        }
                        Msg::None
                    }
                />
            </section>
        )
    }

    fn render_error(&self) -> Html<Model> {
        if let Some(error) = &self.error {
            html!(
                <section class="modal">
                    <mark class="error">
                        <p>{ format!("Error: {}", error) }</p>
                        <a href="#" onclick=|_| { Msg::ClearErr }>{ "x" }</a>
                    </mark>
                </section>
            )
        } else { html!() }
    }
}
